package ukdw.com.androidactivity.CRUD;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.androidactivity.Model.DefaultResult;
import ukdw.com.androidactivity.Network.GetDataService;
import ukdw.com.androidactivity.Network.RetrofitClientInstance;
import ukdw.com.androidactivity.R;

public class MahasiswaAddActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstance){
        super.onCreate(savedInstance);
        setContentView(R.layout.activity_mahasiswa_add);

        final EditText edNama = (EditText)findViewById(R.id.editTextNamaMhs);
        final EditText edNim = (EditText)findViewById(R.id.editTextNim);
        final EditText edAlamat = (EditText)findViewById(R.id.editTextAlamatMhs);
        final EditText edEmail = (EditText)findViewById(R.id.editTextEmailMhs);
        Button btnSimpan = (Button)findViewById(R.id.buttonSimpanMhs);

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon Menunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.add_mhs(
                        edNama.getText().toString(),
                        edNim.getText().toString(),
                        edAlamat.getText().toString(),
                        edEmail.getText().toString(),
                        "kosongkan saja diisi sembarang karean dirandom sistem",
                        "72170136" //diisi nim masing-masing
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(MahasiswaAddActivity.this, "DATA BERHASISL DISIMPAN", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(MahasiswaAddActivity.this, "GAGAL", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}
