package ukdw.com.androidactivity.Crud_Dosen;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.androidactivity.Adapter.DosenRecyclerAdapter;
import ukdw.com.androidactivity.Model.Dosen;
import ukdw.com.androidactivity.Model.Mahasiswa;
import ukdw.com.androidactivity.Network.GetDataService;
import ukdw.com.androidactivity.Network.RetrofitClientInstance;
import ukdw.com.androidactivity.R;

public class DosenGetAllActivity extends AppCompatActivity {
    RecyclerView rvGetDsnAll;
    DosenRecyclerAdapter dsnAdapter;
    ProgressDialog pd;
    List<Dosen> dosenList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dosen_get_all);

        rvGetDsnAll = (RecyclerView)findViewById(R.id.rvGetDsnAll);
        pd = new ProgressDialog(this);
        pd.setTitle("Mohon Bersabar");
        pd.show();

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<List<Dosen>> call = service.getDosen("72170136");


        call.enqueue(new Callback<List<Dosen>>() {
            @Override
            public void onResponse(Call<List<Dosen>> call, Response<List<Dosen>> response) {
                pd.dismiss();
                dosenList = response.body();
                dsnAdapter = new DosenRecyclerAdapter (dosenList);

                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(DosenGetAllActivity.this);
                rvGetDsnAll.setLayoutManager(layoutManager);
                rvGetDsnAll.setAdapter(dsnAdapter);
            }
            @Override
            public void onFailure(Call<List<Dosen>> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(DosenGetAllActivity.this, "Error", Toast.LENGTH_LONG).show();
            }
        });
    }
    }