package ukdw.com.androidactivity.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Mahasiswa {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("nama")
    @Expose
    private String Nama;

    @SerializedName("nim")
    @Expose
    private String Nim;

    @SerializedName("notelp")
    @Expose
    private String NoTelp;

    @SerializedName("alamat")
    @Expose
    private String alamat;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("foto")
    @Expose
    private String Foto;

    @SerializedName("nim_progmob")
    @Expose
    private String nim_progmob;

    public Mahasiswa(String nama, String nim, String noTelp) {
        Nama = nama;
        Nim = nim;
        NoTelp = noTelp;
    }

    public Mahasiswa(String id, String nama, String nim, String noTelp, String alamat, String email, String foto, String nim_progmob) {
        this.id = id;
        Nama = nama;
        Nim = nim;
        NoTelp = noTelp;
        this.alamat = alamat;
        this.email = email;
        Foto = foto;
        this.nim_progmob = nim_progmob;
    }

    public Mahasiswa(String nama, String nim, String noTelp, String alamat, String email, String foto, String nim_progmob) {
        Nama = nama;
        Nim = nim;
        NoTelp = noTelp;
        this.alamat = alamat;
        this.email = email;
        Foto = foto;
        this.nim_progmob = nim_progmob;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return Nama;
    }

    public void setNama(String nama) {
        Nama = nama;
    }

    public String getNim() {
        return Nim;
    }

    public void setNim(String nim) {
        Nim = nim;
    }

    public String getNoTelp() {
        return NoTelp;
    }

    public void setNoTelp(String noTelp) {
        NoTelp = noTelp;
    }
    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFoto() {
        return Foto;
    }

    public void setFoto(String foto) {
        Foto = foto;
    }

    public String getNim_progmob() {
        return nim_progmob;
    }

    public void setNim_progmob(String nim_progmob) {
        this.nim_progmob = nim_progmob;
    }
}
