package ukdw.com.androidactivity.Pertemuan2;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import ukdw.com.androidactivity.Adapter.MahasiswaRecyclerAdapter;
import ukdw.com.androidactivity.Model.Mahasiswa;
import ukdw.com.androidactivity.R;

public class RecyclerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler);

        RecyclerView rv = (RecyclerView)findViewById(R.id.rvLatihan);
        MahasiswaRecyclerAdapter mahasiswaRecyclerAdapter;

        //data dummy
        List<Mahasiswa> mahasiswaList = new ArrayList<Mahasiswa>();

        //generate data mahasiswa
        Mahasiswa m1= new Mahasiswa("Skolastika","72170136","081234567890");
        Mahasiswa m2= new Mahasiswa("Priska","72189901","080987655443");
        Mahasiswa m3= new Mahasiswa("Kenzo","71234556","088765738390");
        Mahasiswa m4= new Mahasiswa("Vigo","72987654","083251637655");
        Mahasiswa m5= new Mahasiswa("Rizky","72673452","089800765456");

        mahasiswaList.add(m1);
        mahasiswaList.add(m2);
        mahasiswaList.add(m3);
        mahasiswaList.add(m4);
        mahasiswaList.add(m5);

        mahasiswaRecyclerAdapter = new MahasiswaRecyclerAdapter(RecyclerActivity.this);
        mahasiswaRecyclerAdapter.setMahasiswaList(mahasiswaList);

        rv.setLayoutManager(new LinearLayoutManager(RecyclerActivity.this));
        rv.setAdapter(mahasiswaRecyclerAdapter);

    }
}